# -*- coding:utf-8 -*-
"""A setuptools based setup module for pytest-faketime.
"""

import setuptools


setuptools.setup(
    name="pytest-faketime",
    version="0.1.0",
    description="pytest plugin to manipulate current time in tests",
    url="https://gitlab.com/vbogretsov/pytest-faketime",
    author="vbogretsov",
    author_email="bogrecov@gmail.com",
    license="BSD",
    packages=setuptools.find_packages(exclude=["env", "tests"]),
    setup_requires=["pytest-runner"],
    tests_require=["pytest"],
    entry_points = {
        "pytest11": [
            "faketime = pytest_faketime",
        ]
    },
    install_requires=["pytest"]
)