# pytest-faketime 0.1.0

Plugin for [pytest](http://pytest.org) to manipulate current time in tests

## Installation

~~~~{.bash}
pip install -e git+https://gitlab.com/vbogretsov/pytest-faketime#egg=pytest_faketime-0.1.0
~~~~

## Usage

```python

import datetime

def test_something(faketime):
    now = faketime.current
    # get time when the fixture instance was created

    now_utc = faketime.current_utc
    # get UTC time when the fixture instance was created

    faketime.current = datetime.datetime(2016, 1, 1)
    # now datetime.datetime.now() in any place will return
    # datetime.datetime(2016, 1, 1)

    faketime.current_utc = datetime.datetime(2016, 1, 1)
    # now datetime.datetime.utcnow() in any place will return
    # datetime.datetime(2016, 1, 1)

```

## Licence

See the LICENSE file