# -*- coding:utf-8 -*-
"""tests for module pytest-faketime.
"""

import datetime


def test_cur(faketime):
    now = datetime.datetime(2016, 1, 1)
    faketime.current = now
    assert datetime.datetime.now() == now


def test_cur_utc(faketime):
    utc_now = datetime.datetime(2016, 1, 1)
    faketime.current_utc = utc_now
    assert datetime.datetime.utcnow() == utc_now
